<h2 class="modal-title">How do you want to feel?</h2>
<img src="{{ theme_asset("images/hero-24772247.png") }}" alt="" class="hero" />
<div class="logo">
	<img src="{{ theme_asset("images/vita-ultra-logo.svg") }}" alt="" />
	<p class="lead">Countless people use Vita Ultra every day as an all–natural method to gently flush away unwanted toxins.</p>
</div>
<div class="content">
	<p>Did you know that as much as 10 pounds of undigested food and fecal matter can be trapped inside your body? Can you imagine what this toxic waste is doing to your health?</p>
	<p>We formulated Vita Ultra specifically to help optimize the natural waste elimination process of your digestive system &mdash; enhancing your overall feeling of good health.</p>
	<p>How? Vita Ultra uses natural herbs and ingredients to help eliminate impacted waste, gently cleansing your body while assisting in the proper absorption of vitamins and minerals.</p>
</div>
<div class="content">
	<ul class="benefit-list icon-list checkmark">
		<li>Purify your system</li>
		<li>Reduce harmful waste</li>
		<li>Reduce gas and bloating</li>
		<li>Improve daily digestion</li>
	</ul>
</div>
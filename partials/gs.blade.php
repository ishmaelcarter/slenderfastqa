<div class="remodal remodal-small remodal-short" data-remodal-id="gs-modal">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div>
	<h1><img class="img-responsive" src="{{ theme_asset("images/gs-logo.png") }}" alt="GuaranteeShip" /></h1>
	<p>With everything we guarantee, it makes no sense to ship with anyone else but GuaranteeShip. We're simply the best! We Insure items to all destinations domestic and international. We offer full replacement value, NOT the depreciated cash value. <small><i>GuaranteeShip is billed separately from shipping &amp; handling.</i></small></p>
    </div>
</div>

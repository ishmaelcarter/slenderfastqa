<div id="billing">
    <h4 class="text-center">Verify Billing Address</h4>
    <hr />
    <div class="form-group col-xs-12 text-center">
        <label for="billingSameAsShipping"><input type="checkbox" name="billingSameAsShipping" id="billingSameAsShipping" value="Y" onchange="$('#billingFields').find('input, select').prop('disabled', function(_, prop){ return ! prop});"> <span class="text-muted">Billing Address is same as Shipping Address</span></label>
    </div>
    <div id="billingFields">
        @include(theme_template('partials/forms/billing'))
    </div>
    <div class="form-group text-center col-xs-12">
        <button class="btn btn-sm btn-default" data-panel="payment">Continue</button>
    </div>
</div>
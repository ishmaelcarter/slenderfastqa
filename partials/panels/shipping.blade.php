<div id="shipping">
    <h4 class="text-center">Verify Shipping Address</h4>
    <hr />
    @include(theme_template('partials/forms/shipping'))
    <div class="form-group text-center col-xs-12">
        <button class="btn btn-sm btn-default" data-panel="payment">Continue</button>
    </div>
</div>

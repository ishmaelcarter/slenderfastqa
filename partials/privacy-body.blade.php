            <h1>Privacy Policy</h1>
            <p><strong>EFFECTIVE DATE:</strong> November 30, 2011</p>

			<p>This Privacy Policy (Policy) discloses the practices of <span>{{ $company->name() }}</span> concerning
				personal information we obtain about you. By using <span>{{ $company->name() }}</span>, in any way
				accessing <span>{{ $company->name() }}</span>, or using other services of
				<span>{{ $company->name() }}</span> (collectively the Services), you are accepting the practices
				described in this Policy.</p>

			<p>This Policy notifies you of: (a) what personal information of yours is collected when you access
				<span>{{ $company->name() }}</span> and/or use the Services or purchase any product; (b) how the
				information is collected; (c) how the information is used and shared; (d) our efforts to maintain the
				security of Sensitive Information; and (e) other details concerning your personal information and our
				policies.</p>

			<p>This Policy does not apply to the practices of companies that <span>{{ $company->name() }}</span> does
				not own or control, or to people that we do not employ or manage.</p>

			<section>
				<h3>Information Collection and Use</h3>

				<p>Information collected by <span>{{ $company->name() }}</span> is stored in a single, secure database,
					accessible only by <span>{{ $company->name() }}</span>. <span>{{ $company->name() }}</span>
					collects information from our paying customers or other persons who use, attempt to use, or purchase
					<span>{{ $company->name() }}</span> products or the services at several different points and from
					different sources, all or some of which may apply to you.</p>
			</section>
			<section>
				<h3>Registration</h3>

				<p>When you sign up as a customer or potential customer, we ask for, and you are required to give, your
					name, address, zip code, telephone number, email address, credit card or checking account number,
					and credit card expiration date. Once you register as a customer with us, you are not anonymous to
					us.</p>
			</section>
			<section>
				<h3>Cookies</h3>

				<p>Cookies are alphanumeric identifiers that we transfer to your computer’s hard drive through your Web
					browser.</p>
			</section>
			<section>
				<h3>Log Files</h3>

				<p><span>{{ $company->name() }}</span> automatically receives and records information on its server logs
					from your Web browser, including your IP address, cookie information, and the page you requested.
					We use IP addresses and cookies to analyze trends, administer the site, track members and users
					movements, and gather broad demographic information for aggregate use. IP addresses are not linked
					to your personal information.</p>
			</section>
			<section>
				<h3>Information from Other Sources</h3>

				<p>for reasons such as improving personalization of our Service (for example, providing better product
					recommendations or special offers that we think will interest you), we might receive information
					about you from other sources and add it to our account information. We also sometimes receive
					updated delivery and address information from our shippers or other sources so that we can correct
					our records and deliver your next purchase or communication more easily.</p>
			</section>
			<section>
				<h2>Use of Information</h2>

				<p><strong>Information we collect is generally used for the following purposes:</strong></p>

				<ol>
					<li>To build features that will make the services available on our site easier to use. This includes
						faster search requests, better member support and timely notice of new services and special offers.
					</li>
					<li>To improve our marketing and promotional efforts, to analyze site usage, improve our content and
						product offerings, and customize our sites content, layout, and services. These uses improve our
						Services and better tailor them to meet your needs.
					</li>
					<li>To resolve disputes, troubleshoot problems and enforce our Terms and Conditions of Use. At
						times, we may look across multiple members or users to identify problems or resolve disputes,
						and in particular we may examine your information to identify members using multiple Member Ids
						or aliases.
					</li>
					<li>Sharing of Information. We may use the personal information that you supply to us and we will
						bring selected retail opportunities to you via direct mail, email, online and telemarketing.
						<span>{{ $company->name() }}</span> may sell or rent aggregated statistical information and
						user, member or former member specific information, including name, address, telephone number
						and payment information, to our marketing partners or other third-parties. If you want
						<span>{{ $company->name() }}</span> to stop sharing information it has about you in the future,
						you may notify us of this fact by contacting <span>{{ $company->name() }}</span>'s Member
						Services department.
					</li>
				</ol>

				<p>We reserve the right to disclose personal information, including Sensitive Information, when we
					believe that such disclosure is appropriate to comply with the law or a request by a government
					official, to protect the rights or property of <span>{{ $company->name() }}</span>, or to enforce
					our Terms and Conditions of Use. In the event that <span>{{ $company->name() }}</span> or
					substantially all of its assets are acquired by a third party, member information and Sensitive
					Information may be some of the transferred assets.</p>
			</section>
			<section>
				<h3>Use of Data Collected</h3>

				<p>We use your personal, demographic and profile data to enhance your experience at our site and to
					enable us to present content we think you might be interested in. We use your contact information to
					send you information about our <span>{{ $company->name() }}</span> and promotional material from our
					partners. We may also use your personal, demographic and profile data to improve our site, for
					statistical analysis, for marketing and promotional purposes, send SMS alerts or notification, and
					for editorial or feedback purposes for our advertisers. Information collected by us may be added to
					our databases and used for future telemarketing, SMS text-messaging, e-mails or postal mailings
					regarding site updates, new products and services, upcoming events, and/or status of orders placed
					online. By using this site, you agree that you may be contacted in any manner contemplated in this
					section even if your number is found on a do not call registry, in-house list or similar
					registry.</p>
			</section>
			<section>
				<h3>SMS Offering and Opt-out rights</h3>

				<p>By completing or submitting a registration form or partial registration form you are consenting to
					receive SMS, wireless or other mobile offering to your cell phone. You understand that your wireless
					carrier's standard charges and rates apply to these messages. For SMS text messages, you may remove
					your information by replying "STOP", "END", or "QUIT" to the SMS text message you have received and
					we will remove your personal information within 10 days of receiving such request.</p>
			</section>
			<section>
				<h3>Agents</h3>

				<p>We employ other companies and individuals to perform functions on our behalf. Examples include
					fulfilling orders, delivering packages, sending postal mail and e-mail, removing repetitive
					information from member lists, analyzing data, providing marketing assistance, processing credit
					card payments, and providing member services. They have access to personal and Sensitive Information
					needed to perform their functions, but may not use it for other purposes.</p>
			</section>
			<section>
				<h3>Special Offers and Announcements</h3>

				<p>Active customers and former customers will occasionally receive information on products, services,
					special deals, and a newsletter.</p>
			</section>
			<section>
				<h3>Security</h3>

				<p><span>{{ $company->name() }}</span> takes every precaution to protect our users and members Sensitive
					Information, both online and offline. Your Sensitive Information is password-protected for your
					privacy and security. Sensitive Information is encrypted and is protected online with the best
					encryption software in the industry - SSL.</p>

				<p>Sensitive Information is also protected offline in our offices. Only employees who need the
					information to perform a specific job (for example, a billing clerk or member services
					representative) are granted access to Sensitive Information. Furthermore, all employees are kept
					up-to-date on our security practices and changes in those practices. Finally, the servers that we
					store Sensitive Information on are kept in a secure environment. If you have any questions about the
					security at our website, you can send an email to us using <span>{{ $company->name() }}</span>
					online contact form.</p>
			</section>
			<section>
				<h3>Other Sites</h3>

				<p><span>{{ $company->name() }}</span> forwards users and members to other sites. Please be aware that
					<span>{{ $company->name() }}</span> is not responsible for the business and privacy practices of
					these other sites. We encourage you to be aware of this when you leave our site and to read the
					legal notices and privacy policies of each and every website you visit.</p>
			</section>
			<section>
				<h3>Correction/Updating Personal Information</h3>

				<p>If a members personal information changes we will endeavor to provide a way to correct or update that
					members personal data provided to us. Visit our Customer Care section to update</p>
			</section>
			<section>
				<h3>Notification of Changes</h3>

				<p>If we decide to change our Policy, we will post these changes on our Homepage or provide other
					notification of our revised Policy so our users and members are always aware of what information
					we collect, how we use it, and when we disclose it.</p>
			</section>
			<section>
				<h3>Children's Online Privacy Protection</h3>

				<p><span>{{ $company->name() }}</span> serves general users of the World Wide Web. We support and comply
					with the Children's Online Protection Act (COPPA) and we do not knowingly collect information from
					children under the age of 13, nor do we share such information with third parties. Children under
					the age of 18 may use <span>{{ $company->name() }}</span> only with the involvement of a parent or
					guardian.</p>
			</section>
			<section>
				<h3>Special Notification for California Residents, Your California Privacy Rights</h3>

				<p>Individual customers who reside in California and have provided their personal information to
					<span>{{ $company->name() }}</span> may request information about our disclosures of certain
					categories of personal information to third parties for their direct marketing purposes. Such
					requests must be submitted to us using <span>{{ $company->name() }}</span> online contact form.
					Within thirty days of receiving such a request, we will provide a list of the categories of personal
					information disclosed to third parties for third-party direct marketing purposes during the
					immediately preceding calendar year, along with the names and addresses of these third parties. This
					request may be made no more than once per calendar year. We reserve our right not to respond to
					requests submitted other than to the address specified in this paragraph.</p>
			</section>
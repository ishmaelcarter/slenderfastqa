<footer class="container">
    <div class="row">
        <div class="col-xs-12">
            <p class="small">&copy; Copyright 2011-2017 Slenderfast.info | <a href="#" data-remodal-target="terms-modal">terms</a> | <a href="#" data-remodal-target="privacy-modal">privacy</a> | <a href="#" data-remodal-target="contact-modal">contact</a></p>
            <p class="small">The products and the claims made about specific products on or through this site have not been evaluated by the FDA and are not approved to diagnose, treat, cure or prevent any disease. The information provided on this site or any information contained on or in any product label or packaging is for informational purposes only and is not intended as a substitute for advice from your physician or other health care professional.  The individuals shown may be paid models, and not necessarily actual {{ $product->name() }} customers.</p>
        </div>
    </div>
    @include(theme_template("partials/google-analytics"))
</footer>

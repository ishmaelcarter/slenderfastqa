<!-- Bootstrap -->
<link href="{{ theme_asset("css/bootstrap.min.css") }}" rel="stylesheet">

<!-- Lil B --><!-- https://github.com/ItsJonQ/lil-b -->
<link href="{{ theme_asset("css/b.css") }}" rel="stylesheet">

<!-- Theme CSS -->
<link href="{{ theme_asset("css/mytheme.css") }}" rel="stylesheet">

<!-- conditional gradient support for IE9 -->
<!--[if gte IE 9]>
<style type="text/css">
    .gradient {
        filter: none;
    }
</style>
<![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Enable prior to deployment <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="3419171af6fedc90a96525c8aba96216"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.11/vue.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/remodal/1.0.3/remodal.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.0.8/jquery.placeholder.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.1.2/parsley.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.3.2/jquery.payment.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ theme_asset("js/sash.js") }}"></script>
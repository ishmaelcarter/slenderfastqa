<div class="form-group col-xs-12">
    <label class="sr-only" for="billingAddress1">Address</label>
    <input type="text" class="form-control" id="billingAddress1" name="billing[address1]" maxlength="100" placeholder="Address (Where to send the product)" value="@if($customer && $customer->billing()){{ $customer->billing()->street1() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter address" >
</div>
<div class="form-group col-xs-12">
    <label class="sr-only" for="billingAddress2">Apt/Unit/Suite (optional)</label>
    <input type="text" class="form-control" id="billingAddress2" name="billing[address2]" maxlength="100" placeholder="Apt/Unit/Ste (optional)" value="@if($customer && $customer->billing()){{ $customer->billing()->street2() }}@endif" data-parsley-trigger="keydown blur" minlength="1">
</div>
<div class="form-group col-sm-8 col-xs-12">
    <label class="sr-only" for="billingCity">City</label>
    <input type="text" class="form-control" id="billingCity" name="billing[city]" placeholder="City" maxlength="50" value="@if($customer && $customer->billing()){{ $customer->billing()->city() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter city" >
</div>
<div class="formRow">
    <div class="form-group col-xs-12 col-sm-4">
        <input type="hidden" name="billing[country]" value="{{ $country }}">
        <select class="form-control" id="billingState" name="billing[state]" data-parsley-trigger="change blur" data-parsley-error-message="Select state" >
            @if($country == "CA")
                @include(theme_template('partials/forms/ca-provinces'))
            @else
                @include(theme_template('partials/forms/usa-states'))
            @endif
        </select>
        @if($customer && $customer->billing())
            <script>
                $(function() {
                    $("#billingState").val("{{ $customer->billing()->state() }}");
                });
            </script>
        @endif
    </div>
</div>
<div class="formRow">
    <div class="form-group col-xs-12 col-sm-4">
        <label class="sr-only" for="billingZip">Zip</label>
        @if($country == "CA")
            <input type="text" class="form-control" id="billingZip" maxlength="6" name="billing[zip]" placeholder="Postal Code" value="@if($customer && $customer->billing()){{ $customer->billing()->zip() }}@endif" data-parsley-trigger="keydown blur submit" data-parsley-error-message="Enter postal code" pattern="/[A-CEGHJ-NPRSTVXY]\d[A-CEGHJ-NPRSTV-Z] ?\d[A-CEGHJ-NPRSTV-Z]\d)$/i" >
        @else
            <input type="tel" class="form-control" maxlength="5" data-parsley-pattern="/^\d{5}$/" id="billingZip" name="billing[zip]" placeholder="Zip" value="@if($customer && $customer->billing()){{ $customer->billing()->zip() }}@endif" data-parsley-trigger="keydown blur submit" data-parsley-error-message="Enter zip" >
        @endif
    </div>
    <div class="form-group col-sm-8 col-xs-12">
        <label class="sr-only" for="billingPhone">Phone (For verification)</label>
        <input type="tel" class="form-control" id="billingPhone" name="phone[1]" placeholder="Phone (For security verification)" data-parsley-trigger="blur" value="@if($customer && isset($customer->phones()[1])){{ $customer->phones()[1] }}@endif" data-parsley-pattern="/^\d{10}$/" data-parsley-error-message="Please enter a valid number" data-parsley-error-message="Enter valid telephone number" >
    </div>
</div>
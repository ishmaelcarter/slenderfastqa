<form id="prospect" action="/prospect" method="post">
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <strong>There seems to be an issue.</strong>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_redirect" value="/order">
    <div class="row">
    	<div class="formRow">
            <div class="form-group col-xs-12 col-sm-6">
                <label class="sr-only" for="shippingFirstName">First Name</label>
                <input type="text" class="form-control" id="shippingFirstName" name="first_name" placeholder="First Name" maxlength="50" value="@if($customer){{ $customer->name()->first() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter first name" required>
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label class="sr-only" for="shippingLastName">Last Name</label>
                <input type="text" class="form-control" id="shippingLastName" name="last_name" placeholder="Last Name" maxlength="50" value="@if($customer){{ $customer->name()->last() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter last name" required>
            </div>
        </div>
        <div class="formRow">
            <div class="form-group col-xs-12">
                <label class="sr-only" for="shippingAddress1">Address (Where to send the product)</label>
                <input type="text" class="form-control" id="shippingAddress1" name="shipping[address1]" maxlength="100" placeholder="Address (Where to send the product)" value="@if($customer){{ $customer->shipping()->street1() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter address" required>
            </div>
        </div>
        <div class="formRow">
            <div class="form-group col-xs-12">
                <label class="sr-only" for="shippingAddress2">Apt/Unit/Suite (optional)</label>
                <input type="text" class="form-control" id="shippingAddress2" name="shipping[address2]" maxlength="100" placeholder="Apt/Unit/Ste (optional)" value="@if($customer){{ $customer->shipping()->street2() }}@endif" data-parsley-trigger="keydown blur" minlength="1">
            </div>
		</div>
        <div class="formRow">
            <div class="form-group col-sm-8 col-xs-12">
                <label class="sr-only" for="shippingCity">City</label>
                <input type="text" class="form-control" id="shippingCity" name="shipping[city]" placeholder="City" maxlength="50" value="@if($customer){{ $customer->shipping()->city() }}@endif" data-parsley-trigger="keydown blur" data-parsley-error-message="Please enter city" required>
            </div>
		</div>
        <div class="formRow">
            <div class="form-group col-xs-12 col-sm-4">
                <input type="hidden" name="shipping[country]" value="{{ $country }}">
                <select class="form-control" id="shippingState" name="shipping[state]" data-parsley-trigger="change blur" data-parsley-error-message="Select state" required>
                    @if($country == "CA")
                        @include(theme_template('partials/forms/ca-provinces'))
                    @else
                        @include(theme_template('partials/forms/usa-states'))
                    @endif
                </select>
                @if($customer)
                <script>
                    $(function() {
                        $("#shippingState").val("{{ $customer->shipping()->state() }}");
                    });
                </script>
                @endif
            </div>
			<div class="clear"></div>
        </div>
        <div class="formRow">
            <div class="form-group col-xs-12 col-sm-4">
                <label class="sr-only" for="shippingZip">Zip</label>
                @if($country == "CA")
                <input type="text" class="form-control" id="shippingZip" maxlength="6" name="shipping[zip]" placeholder="Postal Code" value="@if($customer){{ $customer->shipping()->zip() }}@endif" data-parsley-trigger="keydown blur submit" data-parsley-error-message="Enter postal code" pattern="/[A-CEGHJ-NPRSTVXY]\d[A-CEGHJ-NPRSTV-Z] ?\d[A-CEGHJ-NPRSTV-Z]\d)$/i" required>
                @else
                <input type="tel" class="form-control" maxlength="5" data-parsley-pattern="/^\d{5}$/" id="shippingZip" name="shipping[zip]" placeholder="Zip" value="@if($customer){{ $customer->shipping()->zip() }}@endif" data-parsley-trigger="keydown blur submit" data-parsley-error-message="Enter zip" required>
                @endif
            </div>
            <div class="form-group col-sm-8 col-xs-12">
                <label class="sr-only" for="shippingPhone">Phone (For verification)</label>
                <input type="tel" class="form-control" id="shippingPhone" name="phone" placeholder="Phone (For verification)" data-parsley-pattern="/^\d{10}$/" data-parsley-error-message="Please enter a valid number" data-parsley-trigger="blur" value="@if($customer){{ $customer->phones()[0] }}@endif" required>
            </div>
		</div>
		<div class="formRow">
            <div class="form-group col-xs-12">
                <label class="sr-only" for="shippingEmail">Email (For order confirmation)</label>
                <input type="email" class="form-control" id="shippingEmail" name="email" maxlength="100" placeholder="Email (For order confirmation)" value="@if($customer){{ $customer->email()}}@endif" data-parsley-type="email" data-parsley-error-message="Please enter a valid email" data-parsley-excluded="true" required><div id="emailStatus"></div>
            </div>
		</div>
        <div id="cta" class="form-group col-xs-12">
            <button class="btn btn-primary btn-lg btn-block gradient" type="submit" aria-describedby="privacy-terms" id="rush-my-trial">Submit<span class="glyphicon glyphicon-triangle-right"></span></button>
                            <span class="help-block small text-center">
                            	<a href="#" data-remodal-target="privacy-modal">Privacy Policy</a> &amp;
                            	<a href="#" data-remodal-target="terms-modal">Offer Terms</a></span>
        </div>
        <div class="badge-container">
        	<div class="sprite secure-bag"><span></span></div>
        	<div class="sprite ssl-badge"><span></span></div>
			<div class="sprite secure-payment-badge"><span></span></div>
        </div>
    </div>
    <input type="hidden" name="jsEnabled" id="jsEnabled" value="false" />
</form>

<?php
if (isset($customer)) {
	$selectedState = $customer->shipping()->state();	
} else {
	$selectedState = "";
}

$arrStates = array("AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");
foreach ($arrStates as $state) {
?>
<option value="{{ $state }}">{{ $state }}</option>		
<?php	
}
?>
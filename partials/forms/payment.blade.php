<div class="formRow addressPreview" v-show="step == 1">
	<div id="shippingAddressPreview">
	<strong>Ship To:</strong>
    <span>
    @if($customer){{ $customer->name()->first() }}@endif @if($customer){{ $customer->name()->last() }}@endif<br />
    @if($customer){{ $customer->shipping()->street1() }}@endif<br />
    @if($customer)
    	@if($customer->shipping()->street2())
		    {{ $customer->shipping()->street2() }}<br />
         @endif
    @endif
    @if($customer){{ $customer->shipping()->city() }}@endif, @if($customer){{ $customer->shipping()->state() }}@endif @if($customer){{ $customer->shipping()->zip() }}@endif<br />
    @if($customer)
    <?php 
	$rawNumber = $customer->phones()[0];
	$formattedNumber = "(".substr($rawNumber, 0, 3).") ".substr($rawNumber, 3, 3)."-".substr($rawNumber,6);
	echo $formattedNumber;
	?>
    @endif<br />
    @if($customer){{ $customer->email()}}@endif
    </span>
    <a id="modify-addresses" data-panel="shipping">Edit Shipping Address</a>
    </div>
    <div id="billingAddressPreview">
    <strong>Bill To:</strong>
    <span>Same as shipping.</span>
    <a id="modify-addresses" data-panel="billing">Edit Billing Address</a>
    </div>
	<div class="clear"></div>
</div>
<a name="ccForm"></a>
<div id="acceptance-marks" class="row text-center">
    <div id="visa" class="sprite visa-sm"><span></span></div><div id="mastercard" class="sprite mc-sm"><span></span></div><div id="discover" class="sprite discover-sm"><span></span></div>
</div>
<div class="row">
    <div class="form-group col-xs-12">
        <input type="tel" name="creditCardNumber" id="creditCardNumber" autocomplete="off" placeholder="Credit Card Number:" data-parsley-excluded="true" class="form-control" required>
        <div id="creditCardNumberStatus"></div>
    </div>
</div>
<div class="row">
	    <div class="col-xs-6">
        <input type="tel" class="form-control" name="exp" id="exp" value="" placeholder="Expiration MM / YYYY" autocomplete="off" data-parsley-excluded="true" required />
        <div id="expStatus"></div>
    </div>
    <div class="col-xs-6 right">
        <input type="tel" name="CVV" id="CVV" placeholder="Security Code:" class="form-control" pattern="[0-9]{3,4}" data-parsley-excluded="true" autocomplete="off" required>
        <div id="CVVStatus"></div>
        <a href="#" data-remodal-target="cvv-modal">What's this?</a>
    </div>
</div>
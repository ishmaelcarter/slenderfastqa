        <div id="billing-switch" class="text-center" v-show="step == 1">
    <div class="row">
        <ul data-panel-group class="nav nav-tabs">
            <li class=""><a data-panel="shipping">Shipping&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span></a></li>
            <li class=""><a data-panel="billing">Billing&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span></a></li>
            <li class="active"><a data-panel="payment">Credit Card&nbsp;&nbsp;<span class="glyphicon glyphicon-credit-card"></span></a></li>
        </ul>
    </div>
</div>
<form id="billingform" action="/order" method="post">
    <div v-show="step == 1 || errors.length || !persistCC">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="panels">
            @include(theme_template('partials/panels/shipping'))
            @include(theme_template('partials/panels/billing'))
            @include(theme_template('partials/panels/payment'))
        </div>
        @if($insurance)
            <input type="hidden" name="shippingInsurance" id="shippingInsurance" value="@{{ !insuranceOptOut }}">
        @endif
    </div>
    <div id="cta" class="form-group col-xs-12" v-show="step < 3">
        <button form="billingform" type="submit" class="btn btn-primary btn-lg btn-block gradient linkDisabled" id="submit-order">Submit<span class="glyphicon glyphicon-triangle-right"></span></button>
	</div>
    <div v-show="step == 2" class="clear errorAdj"></div>
    <div v-show="step == 1 || error == 1">
        <div class="form-group col-xs-12">
            <span id="privacy-terms" class="help-block small text-center">
                <a href="#" data-remodal-target="privacy-modal">Privacy Policy</a> &amp; <a href="#" data-remodal-target="terms-modal">Offer Terms</a>
            </span>
        </div>
        <div class="badge-lg-container">
            <div class="sprite secure-bag"><span></span></div>
            <div class="sprite ssl-badge"><span></span></div>
            <div class="sprite secure-payment-badge"><span></span></div>
        </div>
        <input type="hidden" name="creditCardType" id="creditCardType" />
        <input type="hidden" name="jsEnabled" id="jsEnabled" value="false" />
        <input type="hidden" id="redirect" name="_redirect" value="/order">
    </div>
</form>

<section id="main" class="container thank-you" v-show="step == 3">
    <div class="row">
        <div class="col-md-6">
            <img id="vu-logo" class="img-responsive" src="{{ theme_asset("images/vita-ultra-logo.svg") }}" alt="" />
            <h2 class="text-center">Order Completed</h2>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8">
                    <h3>Our proven formula will:</h3>
                    <ul class="benefit-list icon-list checkmark">
                        <li>Purify your system</li>
                        <li>Reduce harmful waste</li>
                        <li>Reduce gas &amp; bloating</li>
                        <li>Improve digestion</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>Cleanse Your Body <span>Today!</span><br /><small>Be aware and be well!</small></h2>
            <p>Congratulations on taking a positive step towards improving your health. We are excited you have chosen our quality products to be a part of your new healthy lifestyle. We are confident  you are being given the edge you need to succeed. You can do this!</p>
            <p>Good health, improved stamina and increased energy are a priceless gift that only you can give yourself. By taking control of your health today, you are creating the future you have always imagined.</p>
            <p class="lead">Thank you for your order!</p>
            <p>Our professional Customer Service Team is available to assist you with any issue or question you may have, 24 hours a day, 7 days a week for your convenience. Your shipment should arrive within 2-5 business days.</p>

            <h3>Customer Service Phone</h3>
            <p>Toll Free 1 (866) 786-8241<br />24/7 Customer Support</p>
        </div>
    </div>
</section>

<div class="remodal remodal-small remodal-short pc-modal" data-remodal-id="pc-modal">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="header">
        <img src="{{ theme_asset("images/pc-header.png") }}" alt="Pay Certify" />
    </div>
    <div class="content">
        <strong>Enjoy the PayCertify Guarantee on the following:</strong>
        <ul class="checked">
            <li>Insurance for the full purchase price</li>
            <li>Rapid claims processing</li>
            <li>Theft protection</li>
            <li>No Deductibles</li>
        </ul>
        Your shipment is Guaranteed with Pay Certify.
    </div>
</div>



<div class="remodal" data-remodal-id="cvv-modal">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div>
	<h4 class="modal-title">Credit Card Security Code</h4>
	<p>A <strong>card security code</strong> is a verification feature used for "card not present" transactions occurring over telephone, fax or Internet in order to reduce credit card fraud.
	</p>
	<div class="row">
		<div class="col-md-6">
			<h4>Visa, Mastercard and Discover</h4>
			<p>The security code is printed on the <strong>back</strong> of the card as shown:</p>
			<figure class="text-center">
				<img src="{{ theme_asset("images/cvv-vmd-sigpanel.png") }}" />
				<figcaption>Three-digit code printed in the signature strip</figcaption>
			</figure>
			<figure class="text-center">
				<img src="{{ theme_asset("images/cvv-vmd-back.png") }}" />
				<figcaption>Three-digit code printed to the right of the signature strip</figcaption>
			</figure>
		</div>
		<div class="col-md-6">
			<h4>American Express</h4>
			<p>The security code is printed on the <strong>front</strong> of the card as shown:</p>
			<figure class="text-center">
				<img src="{{ theme_asset("images/cvv-amex-front.png") }}" />
				<figcaption>Four-digit code printed above the account number</figcaption>
			</figure>
    	</div>
    </div>
</div>

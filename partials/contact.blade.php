<div class="remodal remodal-small" data-remodal-id="contact-modal">
  <button data-remodal-action="close" class="remodal-close">&times;</button>
  <div class=contact-info>
  <h2 class="orange">Contact Us</h2>
  <p>10115 E Bell Road Suite 107-227,</p>
  <p>Scottsdale, AZ 85260</p>
  <p>884-691-3250</p>
  <p class="orange"><a href="mailto:questions@slenderfast.info">questions@slenderfast.info</p>
  <p class="orange"><a href="mailto:questions@slenderboost.info">questions@slenderboost.info</p>
</div>
</div>

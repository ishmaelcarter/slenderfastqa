<div class="remodal" data-remodal-id="privacy-modal">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="terms-modal-body">
  @include(theme_template("partials/privacy-body"))
  </div>
</div>
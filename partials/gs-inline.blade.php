<tr>
    <td colspan="2" style="text-align: right;">

        <a href="#gs-modal" data-remodal-target="gs-modal"><img src={{ theme_asset("images/gship.png") }} width="100" alt="GuaranteeShip" /></a> +
        <span id="guarenteeship" v-show="!insuranceOptOut">@{{ offering.productOffers[1].insurance.price | currency }}</span>
        <span id="guarenteeship" v-else>$0.00</span>
        <br />&nbsp;<i>billed separately</i></small>

    </td>
</tr>

<h1>TERMS &amp; CONDITIONS <br/>
    <small>Revised April 2, 2015</small>
</h1>

<p><strong>Carefully read the terms &amp; conditions below before ordering</strong></p>

<p><strong>Consent to future contact:</strong> As disclosed in our website materials, by providing your
    telephone number to us, you agree to receive customer service and marketing calls, including through the
    possible use of an automatic telephone dialing system, prerecorded and text messages, from us and our
    agents and affiliates, at the telephone/cellular number you provide to us, which you certify is your own
    number. You are not required to consent to any marketing in order to purchase and you may opt-out of our
    marketing at any time.</p>

<p>These Terms and Conditions ("Agreement") constitute a binding written agreement
    between <span>{{ $company->name() }}</span>, and its affiliated entities (collectively "Company", "we", or "us")
    and you ("you" or "Customer"). By making any use of our Websites ("Sites") or any purchase from us, you
    expressly agree to the terms contained herein. IF YOU DO NOT UNDERSTAND AND AGREE TO ALL OF THESE TERMS,
    OR ARE UNDER EIGHTEEN (18) YEARS OF AGE, YOU SHOULD CEASE ALL USE OF OUR SITES AND LOGOFF IMMEDIATELY.
    This Agreement governs your use of our Sites and also of any purchase by you from our affiliates or us.
    BY PROVIDING YOUR TELEPHONE NUMBER, YOU EXPRESSLY AUTHORIZED US TO CALL YOU OR SEND YOU RECORDED
    MESSAGES OR TEXTS ABOUT OUR PRODUCTS AND SERVICES USING AUTOMATED TECHNOLOGY TO THE TELEPHONE/CELLULAR
    NUMBER YOU ENTERED. YOU UNDERSTAND THAT YOU ARE NOT REQUIRED TO GIVE SUCH CONSENT AS A CONDITION OF ANY
    PURCHASE. You consent and agree that your use of a key pad, mouse or other device to select an item,
    button, icon, checkbox, to enter text, or to perform a similar act/action, while using our Sites, for
    the purpose of accessing or making any transactions regarding any agreement, acknowledgment, consent,
    terms, disclosures or conditions, constitutes your signature, including without limitation of the United
    States Electronic Signatures in Global and National Commerce Act, P.L. 106-229 (the "E-Sign Act")
    acceptance and agreement as if actually signed by you in writing. Be advised that all activity and IP
    address information is being monitored. The right to use any product or service you purchase from us is
    personal to you and is not transferable to any other person or entity. We reserve the right to make
    changes to the Sites, policies, and to this Agreement at any time and without notice. YOU SHOULD PRINT A
    COPY OF THIS AGREEMENT AND CHECK BACK FREQUENTLY FOR UPDATES. Your continued use of our Sites or of any
    purchased product or service following any future amendment constitutes your acceptance of any modified
    terms. If you have any questions regarding these terms, please contact Customer Care
    at <span>{{ $company->phone("support") }}</span>.</p>

<section>
    <strong>Legal Disclaimers</strong>

    <p>Any weight loss product purchased should be used under a doctor's direction only, and always with
        appropriate diet and exercise. Diet supplements alone cannot guarantee weight loss if not combined
        with diet and exercise.</p>

    <p>Statements made by Company have not been evaluated by the food and drug administration. These
        products are not intended to diagnose, treat, cure or prevent any illness or disease. Consult with
        your physician for diagnosis or treatment. Use herbs as per instructions and always watch for any
        allergic reactions.</p>

    <p>The information presented on this Site is not presented with the intention of diagnosing any disease
        or condition or prescribing any treatment. It is offered as information only, for use in the
        maintenance and promotion of good health in cooperation with a licensed medical practitioner.</p>

    <p>The FDA does not evaluate or test herbs. </p>

    <p>In the event that any individual should use the information presented on this Site without a licensed
        medical practitioner's approval, that individual will be diagnosing for him or herself.</p>

    <p>No responsibility is assumed by the author, publisher or distributors of this information should the
        information be used in place of a licensed medical practitioner's services. No guarantees of any
        kind are made for the performance or effectiveness of the preparations mentioned on this website.
        Furthermore, this information is based solely on the traditional and historic use of a given herb,
        or on clinical trials that are generally not recognized by any US government agency or medical
        organization.</p>

    <p>This information has not been evaluated by the US Food and Drug Administration, nor has it gone
        through the rigorous double-blind studies required before a particular product can be deemed truly
        beneficial or potentially dangerous and prescribed in the treatment of any condition or disease.</p>
</section>
<section>
    <strong>Third-Party Sites</strong>

    <p>The Company Sites contain links to other Sites, resources and advertisers. Company is not responsible
        for the availability of these external Sites nor does it endorse or is it responsible for the
        contents, advertising, products or other materials made available on or through such external Sites.
        Under no circumstances shall Company be held responsible or liable, directly or indirectly, for any
        loss or damage caused, or alleged to have been caused, to a user in connection with the use of or
        reliance on any content, goods or services available on such external Sites. You should direct any
        concerns to such external Site's administrator or webmaster.</p>
</section>
<section>
    <strong>Site Content</strong>

    <p>Company neither endorses nor is responsible for the accuracy or reliability of any opinion, advice or
        statement on the Sites, nor for any offensive, defamatory or obscene posting made by any user. Under
        no circumstances will Company be liable for any loss or damage caused by your reliance on
        information obtained through the content on the Sites. It is your responsibility to evaluate the
        accuracy, completeness and usefulness of any information, opinion, advice or other content available
        through the Sites. Please seek the advice of professionals, as appropriate, regarding the evaluation
        of any specific information, opinion, advice or other content, including but not limited to
        financial, health, or lifestyle information, opinion, advice or other content.</p>
</section>
<section>
    <strong>Warranties, Limitation on Liability &amp; Indemnification</strong>

    <p>Except as otherwise provided herein, OUR PRODUCTS AND SERVICES ARE PROVIDED "AS IS" WITHOUT ANY
        EXPRESS OR IMPLIED WARRANTY OF ANY KIND, INCLUDING WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY
        PARTICULAR PURPOSE. Company liability hereunder shall in no event exceed an amount equal to the
        amount actually paid by you to Company in the month prior to a claim under this section, regardless
        of the basis for the claim. You understand that this is a significant limitation on your right to
        sue Company and you should not proceed if you do not agree. You agree to defend, indemnify and hold
        harmless Company, its affiliates and their respective directors, officers, employees and agents from
        and against all claims and expenses, including attorneys' fees, arising out of the use by you of the
        Sites, including claims by other users of your equipment, access, products or membership.
        Notwithstanding the foregoing, nothing contained herein shall be construed to limit Company
        liability for its own negligence except where allowed by law.</p>
</section>
<section>
    <strong>Purchases</strong>

    <p><strong>In-Trial Offer:</strong> A trial offer provides the customer an opportunity to try our
        product free of charge for {{ $product->billing()->cycle('rebill')["in"] }} days from date of order,
        paying only shipping and handling fees of
        {{ $currency["symbol"] }}{{ $product->billing()->cycle('initial')["price"] }}
        ({{ $currency["code"] }}). At the conclusion of the trial period, you will be billed the full
        purchase price of {{ $currency["symbol"] }}{{ $product->billing()->cycle('rebill')["price"] }}
        ({{ $currency["code"] }}) and enrolled in the monthly replenishment program. The monthly program
        delivers a replenishment of product every thirty (30) days at the same low price of
        {{ $currency["symbol"] }}{{ $product->billing()->cycle('rebill')["price"] }} ({{ $currency["code"] }}).
    </p>

    <p><strong>Easy-Pay Quarterly Offer:</strong> The easy-pay quarterly offer provides the customer an
        opportunity to receive a quarterly supply of product (3-month supply), billed in three (3)
        installments. Shipping and handling of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle("initial")["price"] }} ({{ $currency["code"] }}) will be charged on date
        of order. The first installment of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle('rebill')["price"] }} ({{ $currency["code"] }}) will be billed 15 days
        from date of order. The second and third installments of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle('rebill')["price"] }} ({{ $currency["code"] }}) will be billed thirty
        (30) and sixty (60) days from first installment date respectively.</p>

    <p><strong>Single Item Purchase:</strong> A single item purchase is a one-time transaction for an
        individual product. This purchase does not include enrollment in the monthly replenishment program.
        Single item purchases are {{ $currency["symbol"] }}
        {{ $product->billing()->cycle('rebill')["price"] }}( {{ $currency["code"] }}) and subject to shipping
        and handling charges of {{ $currency["symbol"] }}{{ $product->billing()->cycle("initial")["price"] }}
        ({{ $currency["code"] }}).</p>

    <p><strong>Bonus Offers:</strong>
    <ul>
        <li>Buy two (2), get one (1) free (3 month supply)</li>
        <li>Buy three (3), get two (2) free (5 month supply)</li>
    </ul>
    <p>These offers do not include enrollment in the monthly replenishment program. Please refer to the
        30-day return policy for special guidelines regarding returns on these offers. Bonus offers are
        subject to shipping and handling charges of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle("initial")["price"] }} ({{ $currency["code"] }}).</p>

    <p>Contact Customer Care at <span>{{ $company->phone("support") }}</span> for additional questions regarding
        purchase types.</p>
</section>
<section>
    <strong>Shipping</strong>

    <p>Within the continental United States, we ship First Class mail. Shipping time is estimated to be
        between 3-5 business days for arrival (excluding weekends and holidays). International orders are
        shipped International Priority Airmail and may take 7-10 business days with the exception of
        unexpected customs delays which we cannot be held responsible for.</p>

    <p>PLEASE CONTACT CUSTOMER CARE AT <span>{{ $company->phone("support") }}</span> FOR SHIPMENTS NOT RECEIVED
        WITHIN 13 DAYS OR PRIOR TO THE TRIAL END DATE. REFUNDS WILL NOT BE ISSUED FOR SHIPMENTS CLAIMED AS
        UNDELIVERED IF NOT REPORTED PRIOR TO THE TRIAL END DATE. When an incorrect or invalid shipping
        address is provided at time of order, and Guarantee Ship has not been purchased, the reshipment MAY
        be subject to the retail shipping and handling charge of {{ $currency["symbol"] }}
        {{ $productOffering->step(1)->restockingFee() }} ({{ $currency["code"] }}).</p>

    @if ( $productOffering->step(1)->isInsured() )
        @if($insurance->name() === "Pay Certify")
            <p><strong>Pay Certify Guarantee :</strong> The coverage insures the parcel and its contents against
                loss or damage from any external cause while in transit, premises utilized by the insured and in
                care, custody, or control of carriers. Underwriters shall be liable for the invoice value of the
                property lost, destroyed, or damaged. The insurance is billed at a cost of {{ $currency["symbol"] }}{{ $insurance->billing()->cycle('initial')["price"] }}({{ $currency["code"] }})
                for the initial trial order and {{ $currency["symbol"] }}{{ $insurance->billing()->cycle('rebill')["price"] }}({{ $currency["code"] }}) for monthly replenishments.

                If you have any questions regarding this policy please contact us.
                By email: claims@paycertify.com
                By Phone: (866)584­7008
                Hours: Mon/Fri, excluding US public holidays 9:00am ­ 5:00pm PST (­8 GMT).
            </p>
        @else
            <p><strong>Guarantee Ship Service:</strong> The Guarantee Ship service is available for the additional
                cost of {{ $currency["symbol"] }}{{ $insurance->billing()->cycle('initial')["price"] }}
                ({{ $currency["code"] }}) for the initial trial order
                and {{ $currency["symbol"] }}{{ $insurance->billing()->cycle('rebill')["price"] }}({{ $currency["code"] }})
                for monthly
                replenishments. Provided you contact us within {{ $product->billing()->cycle('rebill')["in"] }}
                days of your order, of any product not received, you will be eligible to file a "product not
                received" claim and request a reshipment of your order. Limit one (1) reshipment per order.
                Guarantee Ship becomes invalid after reshipment has occurred. If you have questions about Guarantee
                Ship or would like to make a "product not received claim," please contact our Customer Care at
                {{ $company->phone("support") }}. EMAILED REQUESTS WILL NOT BE PROCESSED. This is not an offer to any
                individual residing in any jurisdiction where the offer would be prohibited by law.</p>
        @endif
    @endif
</section>
<section>
    <strong>Cancellation Policy</strong>

    <p>Due to the high volume of orders we receive, we will be unable to stop an order after submitted.
        PLEASE
        ORDER CAREFULLY. On time delivery of your product is extremely important to us, therefore we have
        streamlined our ordering process and your order will immediately be sent to processing and then
        shipped.
        Due to our streamlined process we do NOT ALLOW cancellations after an order has been placed, except
        where
        required by law or otherwise allowed herein. Once an order has been submitted, your credit card will
        be
        charged and in the event that you choose to cancel your order you will be required to follow the RMA
        Cancellation Policies.</p>

    <p><strong>In-Trial RMA (Return Merchandise Authorization) Cancellations:</strong> When a customer
        decides
        not to continue their use of the product, they are required to cancel prior to the trial end date to
        avoid the trial completion billing of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle('rebill')["price"] }} ({{ $currency["code"] }}) and enrollment in
        the monthly replenishment program. Please note you have two options for successful cancellation.
        You may keep the product and receive a one-time billing of {{ $currency["symbol"] }}
        {{ $product->billing()->cycle('rebill')["price"] }}( {{ $currency["code"] }}), or you may return the
        product with prior RMA authorization to avoid further billing. We recommend that all trial order
        cancellations be made two (2) days prior to the trial end date by calling {{ $company->phone("support") }} -
        EMAILED CANCELLATIONS WILL NOT BE ACCEPTED ON TRIAL ORDERS, TO ENSURE CANCELLATION HAS BEEN
        PROCESSED PRIOR TO YOUR TRIAL END DATE.</p>

    <p><strong>Easy-Pay Quarterly Offer Cancellation:</strong> When a customer decides not to continue with
        the easy-pay quarterly offer, they may request a cancellation for the next quarterly billing cycle
        only. Please note that cancellations within a current billing cycle cannot be applied, as the
        customer is in receipt of a three (3) month supply of product. To cancel, please contact Customer
        Care at <span>{{ $company->phone("support") }}</span>.</p>

    <p><strong>Monthly Replenishment Program Cancellation:</strong> When a customer decides to stop their
        monthly replenishment of product, they may contact Customer Care. We recommend that all
        cancellations be made two (2) days prior to the next billing date by
        calling <span>{{ $company->phone("support") }}</span>.</p>
</section>
<section>
    <strong>REVERSALS AND CHARGEBACKS</strong>

    <p>WE CONSIDER CHARGEBACKS AND REVERSALS AS POTENTIAL CASES OF FRAUDULENT USE OF OUR SERVICES AND/OR
        THEFT OF SERVICES AND AS SUCH WILL BE TREATED. WE RESERVE THE RIGHT OF FILING A COMPLAINT WITH THE
        APPROPRIATE LOCAL AND FEDERAL AUTHORITIES TO INVESTIGATE. BE ADVISED THAT ALL ACTIVITY AND IP
        ADDRESS INFORMATION IS BEING MONITORED AND THAT THIS INFORMATION MAY BE USED IN A CIVIL AND/OR
        CRIMINAL CASE(S) AGAINST A CLIENT IF THERE IS FRAUDULENT USE AND OR THEFT OF SERVICES. </p>

    <p><strong>IN THE EVENT THAT A REVERSAL OR CHARGEBACK CLAIM IS FILED WITH THE CARDHOLDER'S BANK, REFUND
            REQUESTS WILL BE DENIED BY OUR RISK MANAGEMENT DEPARTMENT TO PREVENT FRAUDULENT ACTIVITY
            ATTEMPTING TO OBTAIN MULTIPLE REFUNDS</strong></p>
</section>
<section>
    <strong>Credit Card Declines</strong>

    <p>In the event a credit card transaction declines, after product has been shipped or received, and you
        have not exercised your cancellation rights per the terms and conditions, we reserve the right to
        reprocess the transaction in full. This includes the right to resubmit the charge on or about every
        two (2) days from the original declined transaction date and up to three (3) additional attempts
        thereafter. In the event of subsequent credit card declines, you authorize us to resubmit a reduced
        amount from one-half (1/2) or one-third (1/3) of the full purchase price until the full amount is
        obtained.</p>

    <p> Contact Customer Care at <span>{{ $company->phone("support") }}</span> if you have additional questions
        regarding credit card declines.</p>
</section>
<section>
    <strong>30-Day Return Policy</strong>

    <p>We take great pride in the quality of the products we offer and your satisfaction is our ultimate
        goal. In
        the event you need to return an item to us, you have thirty (30) days from the initial date of
        purchase to
        contact us, request a Return Merchandise Authorization (RMA) and return the product for a refund. IN
        ORDER
        TO PROCESS A REFUND, ALL RETURNS MUST BE PRE-APPROVED AND
        ASSIGNED A RETURN MERCHANDISE AUTHORIZATION (RMA) NUMBER. PRODUCT RETURNED WITHOUT PRIOR
        AUTHORIZATION
        (RETURN TO SENDER) WILL FORFEIT REFUND.</p>

    <p><strong>TO BE ELIGIBLE FOR A REFUND ON TRIAL COMPLETION THE ITEM MUST MEET ALL OF THE FOLLOWING
            CRITERIA:</strong></p>
    <ul>
        <li>Limit one (1) return per product, per household</li>
        <li>Return product must be received within our return department with prior RMA (return merchandise
            authorization) approval, attached with returned product, within thirty (30) days of initial
            trial offer order date
        </li>
        <li>Products marked <em><q>Return to Sender</q></em> are not eligible for refunds</li>
        <li>Customer is responsible for all return shipping costs</li>
        <li>We assess a {{ $currency["symbol"] }}{{ $productOffering->step(1)->restockingFee() }}
            ({{ $currency["code"] }}) processing fee on all returns with the exception of cancellations made
            prior to the Trial End Date
        </li>
        <li>Shipping and handling charges are non-refundable</li>
        <li>Repetitive returns are not allowed</li>
        <li>For refunds on Bonus Offers, all items are required to be returned. If all items in the order
            fail to be returned, the value of the free or bonus product will be deducted from any refund
            issued.
        </li>
    </ul>
    <p>We reserve the right to refuse a refund to any customer who repeatedly requests refunds, or who in
        our judgment, requests a refund in bad faith.</p>

    <p>It is strongly suggested that the customer obtains shipping insurance and a tracking number on their
        return shipment as we will not be responsible for packages that fail to arrive back to the return
        address provided.</p>

    <p>Contact Customer Care for an RMA Request at <span>{{ $company->phone("support") }}</span> or
        <a href="{{ $company->email("support") }}">{{ $company->email("support") }}</a>.</p>
</section>
<section>
    <strong>Damaged or Incorrect items</strong>
    <ul>
        <li>In the event that your order arrives damaged, or you receive the wrong item, please call our
            Customer Service Department at <span>{{ $company->phone("support") }}</span> within 24 hours.
        </li>
        <li>We ask that you do not dispose of any damaged products until you contact the Customer Service
            Department for instructions, as we may require the return of the damaged goods.
        </li>
        <li>In the event of a damaged order, we will ship a replacement order promptly.</li>
        <li>If you have ordered incorrectly, we will ship the correct item once we have received the return
            of the incorrect product.
        </li>
        <li>All damaged orders must be reported within five (5) business days of delivery.</li>
    </ul>
    <p>Damaged orders not reported within five (5) business days of delivery confirmation cannot be adjusted
        or credited.</p>
</section>
<section>
    <strong>Choice of Law, Dispute Resolution</strong>

    <p> This Agreement shall be governed by and construed according to the laws of the State of Arizona,
        without giving effect to
        normal choice-of-law and conflict-of-law principles. The parties agree that a party asserting any
        claim
        or dispute regarding this Agreement shall file and litigate such claim/dispute only in a court in
        Arizona,
        and only after the parties have first submitted to and engaged in commercial mediation in Arizona.
        The
        parties will split the cost of the mediation.</p>
</section>
<section>
    <strong>Trademarks</strong>
    <p>The website content, product names, product lines, website names, promotion and offer names, and all
        related trade and service marks are and shall remain the exclusive intellectual property of Company.
        You specifically acknowledge that this Agreement does not confer upon you any interest in or right
        to use any trademark or service mark of Company or its Affiliates, unless you first receive the
        prior written consent of Company, which Company may grant or withhold in its sole discretion.</p>
</section>
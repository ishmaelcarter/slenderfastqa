<div class="remodal" data-remodal-id="terms-modal">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="modal-body" id="terms-modal-body">
  @include(theme_template("partials/terms-body"))
  </div>
  <div class="modal-footer">
          <button
              form="billingform"
              type="submit"
              data-modal-close="terms-modal"
              data-scroll-toggle-bottom="terms-modal-body"
              class="btn btn-primary btn-lg btn-block gradient"
              aria-hidden="true"
              id="submit-order"
        >I AGREE</button>
  </div>
</div>
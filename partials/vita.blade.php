<div class="remodal vita" data-remodal-id="vita-ultra">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="modal-body" id="terms-modal-body">
  @include(theme_template("partials/vita-body"))
  </div>
</div>
<tr>
    <td colspan="2" style="text-align: right;">
        <a href="#pc-modal" data-remodal-target="pc-modal"><img src={{ theme_asset("images/pc-logo.png") }} width="100" alt="GuaranteeShip" /></a> +
      	<span id="guarenteeship" v-show="step === 1">@{{ offering.productOffers[1].insurance.price | currency }}</span>
      	<span id="guarenteeship" v-show="step === 2">@{{ offering.productOffers[2].insurance.price | currency }}</span>
        <br />&nbsp;<i>billed separately</i></small>

    </td>
</tr>

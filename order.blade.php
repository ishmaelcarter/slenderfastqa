<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="nofollow, noindex" name="robots">
    <title>Slimera Garcinia Cambogia</title>

    <!-- Assets -->
    @include(theme_template("partials/assets"))
</head>
<body data-modal="exit-pop" id="app">
	<div id="process" v-show="processing"><span>Processing<strong>Please Wait</strong></span></div>
    <a name="mainBody"></a>
    <section id="main" class="container" v-show="step < 3">
        <div class="row">
            <div class="col-md-6">
                <!-- Start Slimera Content Panel -->
                <div v-show="step == 1">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="text-center">You're Almost Done!<br /><small>Pay for Just Shipping Today</small></h2>
                            <table id="order-summary" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr><th>Product</th>
                                    <th class="text-right">Price</th></tr>
                                </thead>
                                <tbody>
                                <tr class="lineitem">
                                    <td>
                                        <div class="media">
                                            <a class="pull-left"></a>
                                            <div class="media-body"><h4 class="media-heading">SlenderFast</h4><p>14-Day Risk Free Trial</p></div>
                                        </div>
                                    </td>
                                    <td class="text-right" style="opacity: .8; text-decoration: line-through">@{{ cart.shipping  | free }}</td>
                                </tr>
                                <tr class="lineitem"><td>Shipping &amp; Handling</td>
                                    <td class="text-right">@{{ cart.items[0].price | currency }}</td >
                                </tr>
                                <tr class="lineitem discount"><td>Discount Activated!</td>
                                    <td class="text-right">-&nbsp;@{{ cart.discount | currency }}</td>
                                </tr>
                                <tr class="total"><td>Total</td>
                                    <td class="total text-right">@{{ cart.total | currency }}</td>
                                </tr>
                                @if($insurance)
                                    @if($insurance->name() === "Pay Certify")
                                        @include( theme_template("partials/pc-inline"))
                                    @else
                                        @include( theme_template("partials/gs-inline"))
                                    @endif
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row shipping">
                        <div class="col-xs-12">
                            <div class="bg-info text-info text-center"><div class="sprite usps"><span></span></div><div class="shipDesc">shipped directly to you by our preferred provider</div></div>
                        </div>
                    </div>
                    <div id="disclosure" class="row small fineprint">
                        <div class="col-xs-12">
                            <p>We are so confident that you will love SlenderFast that we are offering a one-time trial offer for you to sample our product. Initially, just pay four dollars ninety-seven cents for shipping and handling to fully evaluate SlenderFast for 14 days. If you are unhappy with the product simply call 1 (866) 786-8241 to cancel your membership. If you like SlenderFast, then do nothing. We will bill eighty-four dollars ninety-seven cents upon the completion of your initial order and eighty-nine dollars ninety-seven cents for every fresh 30-day supply thereafter. Thank you for your order!</p>
                        </div>
                    </div>
                    <!-- End Slimera Content Panel -->
                </div>
                <!-- Start Vita Ultra Content Panel -->
                <div v-show="step == 2">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>Thanks for your order, <span>don't forget...</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <img id="vita-ultra" src={{ theme_asset("images/vita-ultra.png") }} />
                        </div>
                        <div class="col-xs-8">
                            <ul class="benefit-list icon-list checkmark">
                                <li><span>100%</span> Natural</li>
                                <li><span>Purify</span> your system</li>
                                <li><span>Reduce</span> harmful waste</li>
                                <li><span>Reduce</span> gas and bloating</li>
                                <li><span>Improve</span> daily digestion</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="lead text-center"><a href="#" data-remodal-target="vita-ultra">Click Here to Learn More</a></p>
                        </div>
                    </div>
                </div>
                <!-- End Vita Ultra Content Panel -->
            </div>
            <div class="col-md-6"><!--form goes in here-->
				<!-- Start Slimera Order Form -->
                <div v-show="step == 1">
                    <a name="billing-information"></a>
                    <h2 class="text-center">Final Step <small>Billing Information</small></h2>
                    @if($errors->any())
                            <script>
                                location.hash = "#billing-information";
                            </script>
                            <div class="alert alert-danger" role="alert">
                            <strong>There seems to be an issue.</strong>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div v-show="errors.length">
                        <div class="alert alert-danger" role="alert">
                            <strong>There seems to be an issue.</strong>
                            <ul>
                                <li v-for="error in errors">@{{ error.message }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Slimera Order Form -->
                <!-- Start Vita Ultra Cart -->
                <div v-show="step == 2">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>Vita Ultra <small>Cleanse your body today!</small></h2>
                            <div v-show="errors.length">
                                <div class="alert alert-danger" role="alert">
                                    <strong>There seems to be an issue.</strong>
                                    <ul>
                                        <li v-for="error in errors">@{{ error.message }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="order-summary" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th class="text-right">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="lineitem">
                                    <td>
                                        <div class="media">
                                            <a class="pull-left"><img width="50" src={{ theme_asset("images/vita-ultra-sm.png") }} alt="" /></a>
                                            <div class="media-body"><h4 class="media-heading">Vita Ultra</h4><p>18-Day Trial</p></div>
                                        </div>
                                    </td>
                                    <td class="text-right" style="font-weight: bolder; text-decoration: line-through">@{{ cart.shipping | free }}</td>
                                </tr>

                                <tr class="lineitem"><td>Shipping &amp; Handling</td>
                                    <td class="text-right">@{{ cart.subtotal | currency }}</td>
                                </tr>
                                <tr class="lineitem discount"><td>Discount Activated!</td>
                                    <td class="text-right">-&nbsp;@{{ cart.discount | currency }}</td>
                                </tr>
                                <tr class="total"><td>Total</td>
                                    <td class="total text-right">@{{ cart.total | currency }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        @if($insurance)
                                            @if($insurance->name() === "Pay Certify")
                                                @include( theme_template("partials/pc-inline"))
                                            @else
                                                @include( theme_template("partials/gs-inline"))
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Vita Ultra Cart -->
                @include(theme_template('partials/forms/order'))
            </div>
        </div>
    </section>
    @include( theme_template("partials/thank-you"))
    @include( theme_template("partials/footer"))

    @include( theme_template("partials/vita"))

    @include( theme_template("partials/terms"))
    @include( theme_template("partials/privacy"))
    @include( theme_template("partials/contact"))

    @if($insurance)
        @if($insurance->name() === "Pay Certify")
            @include( theme_template("partials/pc"))
        @else
            @include( theme_template("partials/gs"))
        @endif
    @endif

    @include( theme_template("partials/cvv"))

    {{-- */
    $incentive = new \Worldwide\Russell\Products\Incentives\AmountOff(2.49);
    /*
--}}
    @if($cart->canApplyIncentive($incentive) && !$cart->hasAppliedIncentive($incentive))
	<div class="remodal exit-pop" data-remodal-id="exit-pop">
	<button data-remodal-action="close" class="remodal-close"></button>
		<div class="exit-pop-content">
		<h1>Wait</h1>
		<h3>Only $2.48</h3>
			<div id="coupon">
				<h1>$2.49 off</h1>
				<h3>Order now for only $2.48</h3>
				<h5>You are only 1 step away from the body of your dreams!</h5>
			</div>
			<button data-remodal-action="close" v-on:click="applyIncentive('amount', 2.49)" class="btn btn-primary btn-lg btn-block gradient">Apply Coupon</button>
        </div>
    </div>
    @endif
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script>
    $('#billingSameAsShipping').trigger('click');

</script>
<script>
    Vue.filter('free', function (value) {
        return "$84.97";
    });

    var vm = new Vue ({
    el: 'body',
    data: {
        processing: 0,
        offering: {!! $productOffering->toJson() !!},
        cart: {!! $cart->toJson() !!},
        step: {!! $visitorSession->currentStep() !!},
        insuranceOptOut: false,
        persistCC: false,
        errors: [],
        pixels: []
    },
    methods: {
        "applyIncentive": function(type, amount){
            $.post('/api/incentive',
                {
                    '_token': "{{ csrf_token() }}",
                    type: type,
                    amount: amount
                },
                'json')
                .done(function(data){
                    if(data.success){
                        this.cart = data.cart;
                    }
                }.bind(this));
        }
    }
});


</script>
    @include('global.pixels')
</body>
</html>

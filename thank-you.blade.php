<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="nofollow, noindex" name="robots">
    <title>Slimera Garcinia Cambogia</title>

    <!-- Assets -->
    @include(theme_template("partials/assets"))
</head>
<body>

@include( theme_template("partials/thank-you"))

<footer class="container">
    <div class="row">
        <div class="col-xs-12">
            <p class="small">&copy; Copyright 2011-2015 @if($offer->domain()){{ $offer->domain() }} @endif</p>
            <p class="small">The products and the claims made about specific products on or through this site have not been evaluated by the FDA and are not approved to diagnose, treat, cure or prevent any disease. The information provided on this site or any information contained on or in any product label or packaging is for informational purposes only and is not intended as a substitute for advice from your physician or other health care professional.  The individuals shown may be paid models, and not necessarily actual {{ $offer->domain() }} customers.</p>
        </div>
    </div>
    @include(theme_template("partials/google-analytics"))
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ theme_asset("js/bootstrap.min.js") }}"></script>

@while($visitorSession->hasPixelsToFire())
    {!! $visitorSession->nextPixelToFire() !!}
@endwhile
<script>
    var vm = new Vue ({
        el: 'body',
        data: {
            step: 3
        }
    });
</script>
</body>
</html>
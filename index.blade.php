<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="nofollow, noindex" name="robots">
    <title>SlenderFast/SlenderBoost</title>

    <!-- Assets -->
    @include(theme_template("partials/assets"))
</head>
<nav>
  <div class="header-wrapper">
    <div class="header-left">
      <img class='logo' src="{{ theme_asset("images/SlenderFast-Logo.svg") }}" width="200" height="40">
    </div>
    <div class="header-right">
      <ul>
        <li><a href="#testimonials">Testimonials</a></li>
        <li><a href="#why">About</a></li>
        <li class="FreeTrial"><a href="#trial">Free Trial</a></li>
    </div>
  </div>
</nav>
<body data-modal="exit-pop">
	<div id="process" v-show="processing"><span>Processing<strong>Please Wait</strong></span></div>
    <section id="above-the-fold" class="container">
        <div class="row hero_img"><!-- container for above the fold content plus the form -->
          <div class='hero_img'>
            <p>Slender up. </br>Find slim results. </br>Feel your boost.</p>
          </div>

            </div>

    </section>

    <section id="what" class="container">
            <div class="row">
                <div class="col-md-6 process l-col">
                    <img src="{{ theme_asset("images/Both-Product-Bottles.jpg") }}">
                </div>
                <div class="col-md-6 r-col">
                          <div class="process-content">
            <h2 class="orange">100% natural products. Top quality ingredients.
              Our combination is the natural path.</h2>
            <p>SlenderFast utilizes it’s main ingredient, Garcinia Cambogia, to naturally change your metabolism and run with it. The most talked about pumpkin shaped fruit from Southeast Asia and India contains a key ingredient HCA (Hydroxycitric Acid), which is extracted from the rind of the fruit.</p>
            <br>
            <p>SlenderBoost partners Garcinia Cambogia with Green Coffee Beans for the extra push you need to burn calories efficiently. Whether it’s during travels, pre-workout, during workout, or just needed — this is the ideal match made for your good-looking future.</p>
            <ul>
              <li class="FreeTrial"><a href="#trial">Free Trial</a></li>
            </ul>
            </div>
        </div></div></section>

    <section id="why" class="container how">
        <div class="row"><!-- container for two desktop columns -->
            <div class="col-md-12">
              <h2>How It Works</h2>
                <p>Our revolutionary weight loss formula helps you burn fat quicker with the help of our secret ingredient, Garcinia Cambogia. Our proprietary blend of natural weight loss ingredients has been proven to aid in metabolizing fat faster than dieting and exercise alone. Start on the path to feeling and looking great today.</p>
            </div>
            <div class="col-xs-4">
                                <img src="{{ theme_asset("images/running-shoe-01.svg") }}" width="100" height="85">
                <p>SlenderFast and SlenderBoost work best when coupled with a safe exercise routine</p>
                        </div>
                        <div class="col-xs-4">
                                <img src="{{ theme_asset("images/apple-01.svg") }}" width="100" height="85">
                <p>Combined with a healthy diet, Slender Fast and Slender Boost can excell your weight loss.</p>
                        </div>
                        <div class="col-xs-4">
                                <img src="{{ theme_asset("images/leaf-01.svg") }}" width="100" height="85">
                <p>Due to natural components in Garcinia Cambogia, you are more likely to lose weight and burn fat.</p>
                        </div>
        </div>

    </section>

    <section id="testimonial" class="container">
      <div class="row">
                  <div class="col-md-6">
                      <h2 class="orange">Before and After</h2>
                      <p>"SlenderFast and SlenderBoost really helped me achieve my weight loss goals. Dieting and exercising just wasn't doing the trick, but once I tried SlenderFast the weight started coming off. SlenderFast helped me lose weight and SlenderBoost helped keep it off. Now I'm down 25 pounds and feeling better than ever."</p>
              <br>
              <p class="quote">- Jennifer M.</p>
                  </div>
                  <div class="col-md-6">
                      <img src="{{ theme_asset("images/fake-person.jpg") }}">
                  </div>
              </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body bg-warning"><p class="small">*This is a REAL SUCCESS STORY of a REAL CUSTOMER and not a paid model! These weight loss results are not typical. The typical weight loss that could be expected can range from 5 lbs. to 30 lbs. in 4 weeks. Results greatly improve when committed to a regimen of calorie restricted diet and regular exercise.</p></div>
                </div>
            </div>
        </div>
    </section>
    <section id="trial" class="container">
      <div class="row">
    <div class="col-md-6">
      <img src="{{ theme_asset("images/Both-Bottles-Overlap.png") }}">
        </div>
      <div class="col-md-6">
          <h2 class="row">Start Your Free Trial Today!</h2>
          <p>Recieve a free trial of both SlenderFast and SlenderBoost in your free trial offer. Get 20% off your first order of SlenderFast and SlenderBoost after your trial period has expired in 30 days.</p>
          @include( theme_template('partials/forms/prospect') )
      </div>
    </div>
    </section>

    <section id="reference" class="container">
        <div class="row">
            <div class="col-xs-12">
            	<h4>References:</h4>
                <ol>
                    <li><a target="_blank" href="http://www.healthline.com/health/green-coffee-bean-weight-loss">What is green coffee bean extract?</a></li>
                    <li><a target="_blank" href="http://pubs.acs.org/doi/abs/10.1021/jf010753k">Chemistry and Biochemistry of (−)-Hydroxycitric Acid from Garcinia</a> </li>
                    <li><a target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0027510705002630">Bioefficacy of a novel calcium–potassium salt of (−)-hydroxycitric acid</a></li>
                    <li><a target="_blank" href="http://www.sciencedirect.com/science/article/pii/S0271531703002215">Efficacy of a novel, natural extract of (–)-hydroxycitric acid (HCA-SX) and a combination of HCA-SX, niacin-bound chromium and Gymnema sylvestre extract in weight management in human volunteers: a pilot study</a></li>
                    <li><a target="_blank" href="http://www.currenttherapeuticres.com/article/S0011-393X%2803%2900152-8/abstract">Effects of garcinia cambogia (Hydroxycitric Acid) on visceral fat accumulation: a double-blind, randomized, placebo-controlled trial</a></li>
                </ol>
            </div>
        </div>
    </section>

    @include( theme_template("partials/footer"))

    @include( theme_template("partials/terms"))
    @include( theme_template("partials/privacy"))
    @include( theme_template("partials/contact"))
    {{-- */
        $incentive = new \Worldwide\Russell\Products\Incentives\AmountOff(2.49);
        /*
    --}}
    @if($cart->canApplyIncentive($incentive) && !$cart->hasAppliedIncentive($incentive))
	<div class="remodal exit-pop" data-remodal-id="exit-pop" id="exit-pop">
	<button data-remodal-action="close" class="remodal-close"></button>
		<div class="exit-pop-content" id="exit-pop-content">
		<h1>Wait</h1>
		<h3>Only $2.48</h3>
			<div id="coupon">
				<h1>$2.49 off</h1>
				<h3>Order now for only $2.48</h3>
				<h5>You are only 1 step away from the body of your dreams!</h5>
			</div>
            <button data-remodal-action="close" v-on:click="applyIncentive('amount', 2.49)" class="btn btn-primary btn-lg btn-block gradient">Apply Coupon</button>
        </div>
    </div>
    @endif
<script>
    var vm = new Vue ({

        el: 'body',
        data: {
            processing: 0
        },
        methods: {
            "applyIncentive": function(type, amount){
                console.log(arguments);

                $.post('/api/incentive',
                    {
                        '_token': "{{ csrf_token() }}",
                        type: type,
                        amount: amount
                    },
                'json');
            }
        }
    });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
</body>
</html>

var EmailStatus = $('#emailStatus');
var sashDebugList = {
	warnings: [],
	errors: []
};

var Sash = function () {
	// Make sure billing/shipping checkbox is checked
	var billingSameAsShipping = $("#billingSameAsShipping");
	if (billingSameAsShipping.length) {
		billingSameAsShipping.prop('checked', true);
	}


	if( $('input, textarea').placeholder ) {
		$('input, textarea').placeholder();
	}

	var dataLineitem = $('[data-lineitem]');
	var DomFields = function () {

		var fields = {};
		var that = this;

		// set a field
		this.set = function(field, value){
			if(typeof value === 'number'){
				fields[field].rawValue = value;
			} else {
				fields[field].rawValue = numeral().unformat(value);
			}
			return this;
		};

		// populate fields collection with arguments
		dataLineitem.each(function(a, b){
			var jObj = $(b);
			if(!jObj){
				sashDebugList.errors.push('DomFields(): dom id ' + jObj.attr('id') + 'not found');
			}
			jObj.css('white-space', 'nowrap');
			var id = jObj.attr('data-lineitem');
			fields[id] = jObj;
			that.set(id, jObj.text());
		});

		// apply field formatting
		this.render = function(){

			var sum = numeral();
			for(var i in fields){

				// field is empty
				if( !fields[i].rawValue ){
					fields[i].text(fields[i].attr('data-lineitem-empty') || 0);
					continue;
				}
				fields[i].text(numeral(fields[i].rawValue).format('$0,0.00'));
				sum.add(fields[i].rawValue);

			}

			// blind set the total field
			$('[data-lineitem-total]').text(sum.format('$0,0.00'));

		};

		// render fields
		this.render();
	};

	var EmailStatus = $('#emailStatus');
	var InputEmail = $('input[type=email]');
	var validEmail = false;

	function validateEmail(email, form) {
		if (form === "order") {
			valType = "prospect";
		} else {
			valType = "order";
		}

		InputEmail.removeClass("parsley-error");
		EmailStatus.hide();
		$.ajax({
			type: "GET",
			async: true,
			url: "https://api.mailgun.net/v2/address/validate",
			data: { api_key: "pubkey-2a98b56937b626f3dae04bbab8a3cda6", address: email },
			dataType: "jsonp",
			timeout: 3000, // in milliseconds
			crossDomain: true,
		}).done(function( data ) {
			if (data.is_valid === true && $("#" + form).parsley().isValid() === true) {
				hideEmailError();
				validEmail = true;
				/* GA EVENT */
				ga('send', 'event', valType, 'email-pass', InputEmail.val());
				ajaxSubmit(form);
			} else {
				showEmailError();
				vm.processing = 0;
				/* GA EVENT */
				ga('send', 'event', valType, 'email-fail', InputEmail.val());
				validEmail = false;
			}
		}).fail(function() { // If something goes wrong reaching mailgun, just say yes
			hideEmailError();
			if ($("#" + form).parsley().isValid() === true) {
				validEmail = true;
				/* GA EVENT */
				ga('send', 'event', valType, 'email-timeout', InputEmail.val());
				ajaxSubmit(form);
			}
		});

	}

	setLocation();
	sendGAStepEvent();

	function ajaxSubmit(formId)
	{
		var form = $('#' + formId);

		if(formId == "prospect")
		{
			form.unbind('submit').submit();
			return true;
		}

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				'Accept': "application/json"
			},
			type: "POST",
			url: "/api/order",
			data: form.serialize(),
			statusCode: {
				400: function(data){
					//Update fields to show errors.
					var errors = data.responseJSON.errors;
					vm.errors = errors;
					vm.processing = 0;
					setLocation();
				}
			}
		}).done(function(data) {
			vm.persistCC = true;
			vm.cart = data.cart;
			vm.customer = data.customer;
			vm.errors = [];
			vm.step = data.step;
			vm.pixels = data.pixels;
			vm.processing = 0;
			if (vm.step == 2) {
				$("#redirect").val("/thank-you");
			}
			processPixels();
			setLocation();
			sendGAStepEvent();
		}).error(function(data){

		});
	}

	function setLocation() {
		if (vm.step < 3) {
			if (vm.errors.length) {
				location.href = "#mainBody";
			} else if (vm.step == 2) {
				if (vm.persistCC == false) {
					location.href = "#ccForm";
				} else {
					location.href = "#mainBody";
				}
			}
		} else {
			if (typeof vm.step != "undefined") {
				location.href = "#mainBody";
			}
		}
	}

	// Send GA event based on step
	function sendGAStepEvent() {
		if (vm.step == 1) {
			ga('send', 'event', 'order', 'step', '1');
		} else if (vm.step == 2) {
			ga('send', 'event', 'order', 'step', '2');
		} else if (vm.step == 3) {
			ga('send', 'event', 'order', 'step', '3');
		}
	}

	function processPixels()
	{
		while(vm.pixels.length)
		{
			var pixel = vm.pixels.shift();
			var pixelContainer = document.createElement('div');
			pixelContainer.innerHTML = pixel;
			var pixelElement = pixelContainer.firstChild;

			if(pixelElement.tagName === "SCRIPT"){
				$.getScript(pixelElement.src);
			} else if(pixelElement.tagName === "IFRAME"){
				document.getElementById('app').appendChild(pixelElement);
			}
		}

		return;
	}

	$("form").on("submit", function(event) {
		var form = $(this).closest("form").attr('id');
		// If we haven't already validated the email, stop submission and check it
		// We also check to be sure an email field is even present
		if (validEmail !== true) {
			if (InputEmail.length) {
				event.preventDefault();

				var email = $('input[type=email]').val();
				if (!email.length || !isValidEmailAddress(email)) {
					showEmailError();
				} else {
					validateEmail(email, form);
				}

			}

		} else { // Otherwise show processing
			event.preventDefault();
			vm.processing = 1;
			ajaxSubmit(form);
		}
	});

	function showEmailError() {
		EmailStatus.show();
		EmailStatus.html('<span class="error">Please enter a valid email</span>');
		InputEmail.removeClass("parsley-success").addClass("parsley-error");
		vm.processing = 0;
	}


	function hideEmailError() {
		EmailStatus.hide();
		InputEmail.addClass("parsley-success").removeClass("parsley-error");
	}

	// Simple regex email check
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

	// Instantiate parlsey for any form we find
	var form = $("form");

	if (form.length) {
		form.parsley();

		/* GA EVENT */
		window.Parsley.on('field:error', function() {
			var formId = form.attr("id");
			var valType = "";
			if (formId === "order") {
				valType = "prospect";
			} else {
				valType = "order";
			}

			valType += "-validation";

			var fieldName = this.$element[0]["id"] + "-fail";
			ga('send', 'event', valType, fieldName, this.$element[0]["value"]);
		});
	}


	// Mask all telephone fields
	var InputPhones = $("input[id*='Phone']");

	// Temporary phone field handling
	// Remove non-numeric characters
	InputPhones.on("blur keyup submit change", function() {
		var tempVal = $(this).val().replace(/\D/g,'');
		var firstChar = tempVal.charAt(0);
		if (firstChar === "1") {
			tempVal = tempVal.slice(1,11);
		} else {
			tempVal = tempVal.slice(0,10);
		}

		$(this).val(tempVal);

	});

	/*
	 if (InputPhones.length) {
	 InputPhones.mask("(999) 999-9999");
	 }*/

	// Don't fire exit pop on submit action
	$('form').submit(function(){

		$(window).off('beforeunload');

	});

	var InputSubmit = $('[type=submit]');
	var Process = $("#process");
	// Display processing window for non-order pages
	InputSubmit.on('click', function() {
		var form = $(this).closest("form").attr('id');

		var buttonId = $(this).prop("id");

		// Do not display processing window in submit order / apply coupon cases
		// submit order is handled below, apply coupon does not need it
		if (buttonId !== "submit-order" && buttonId !== "apply-coupon") {
			var isValidForm = true;
			// Display processing overlay
			vm.processing = 1;

			// check for invalid required fields
			if(!$("#" + form).parsley().isValid()){
				isValidForm = false;
				// if you find an error, switch to the tab and hide processing window
				vm.processing = 0;
			}

		}

	});
	var dataPanelGroup = $('[data-panel-group]');

	var tabPanels = function(){


		var allClickActionItems = $('[data-panel]');

		if(!$('[data-panel-group] [data-panel]').length){
			sashDebugList.errors.push('tabPanels(): data-panels not found in data-panel-group DOM element');
		}

		if(!dataPanelGroup.length){
			sashDebugList.errors.push('tabPanels(): dataPanelGroup not found in DOM');
		}

		if(!allClickActionItems.length){
			sashDebugList.errors.push('tabPanels(): data-panels not found in DOM');
		}

		var panelItems = null;

		var tmp = [];
		allClickActionItems.each(function(a, b){
			var currentDataPanel = $(b).attr('data-panel');
			if(!$('#' + currentDataPanel).length){
				sashDebugList.errors.push('data-panel: \"' + currentDataPanel + '\" defined but DOM element not found');
			}

			tmp.push('#' + $(b).attr('data-panel'));
		});
		var allPanelsCSSQuery = tmp.join();

		panelItems = $(allPanelsCSSQuery);

		// applies to all buttons in the tab panels except for the submit buttons
		panelItems.find('button').not('[type=submit]').on('click', function(e){
			e.preventDefault();
		});

		var setActiveTab = function(showPanelId){
			// hide all the panels
			panelItems.hide();

			// turn off validation for required form fields in the other tab panels
			$('[required]').not('#' + showPanelId + ' [required]').bind('invalid', function() {
				return false;
			});

			// turn on form validation for required form fields on the active tab
			$('#' + showPanelId + ' [required]').unbind('invalid');

			var rushMyOrder = $('#rush-my-order');
			// If the active panel is payment, show the checkout button
			if (showPanelId === "payment") {
				rushMyOrder.show();
			} else { // otherwise hide it
				rushMyOrder.hide();
			}

			// show active panel
			$('#' + showPanelId).show();

			// remove active class from all clickable tabs - grab parent li and add active class to it
			dataPanelGroup
				.find('li')
				.removeClass('active');

			// mark active item - grab parent li and add active class to it
			dataPanelGroup
				.find('[data-panel='+ showPanelId +']')
				.parents('li')
				.addClass('active');

		};

		var tabsDisplayed = false;
		var showPanelId = "";

		var clickHandler = function(){
			if (!tabsDisplayed) {
				showPanelId = "payment";
				tabsDisplayed = true;
			} else {
				showPanelId = $(this).attr('data-panel');
			}

			setActiveTab(showPanelId);

		};

		allClickActionItems.bind('click', clickHandler).first().trigger('click');

		// do form field validation check / bug fix
		$('[type=submit]').on('click', function() {

			// Display processing
			vm.processing = 1;
			// loop through each tab
			panelItems.each(function(){
				var panel = $(this);
				panel.find('[required]').each(function(){
					var field = $(this)[0];
					var fieldId = $(field).attr('id');
					// check for invalid required fields
					if (!$("#" + fieldId).parsley().isValid() || $("#" + fieldId).hasClass("parsley-error")) {
						// if you find an error, switch to the tab and hide processing window
						setActiveTab(panel.attr('id'));
						vm.processing = 0;
					}
				});

			});

		});

	};

	var exitpopup = function (tag) {

		var jTag = $(tag);
		var cfg = {};
		var modalId = jTag.attr('data-modal');
		cfg.confirmMsg = jTag.attr('data-exit-msg') || "are you sure?";

		var loader = function () {

			// Check if incentive has been applied

			// Spawn exit pop
			/* GA EVENT */
			ga('send', 'event', 'interactions', 'exit-pop', "");

			var inst = $("[data-remodal-id=" + modalId + "]").remodal();
			inst.open();

			//turn this loader func off to make sure exitpop is only fired once
			$(window).off('beforeunload');

			// Interrupt exit process
			return cfg.confirmMsg;

		};

		$(window).bind('beforeunload', loader);

	};



	var popup = function (tag) {
		var jTag = $(tag);
		var modalId = jTag.attr('data-remodal-target');
		var loader = function (e) {
			e.preventDefault();

			var triggerTag = $(this);

			/* GA EVENTS */
			switch(modalId) {
				case "terms-modal":
					ga('send', 'event', 'interactions', 'terms', "");
					break;
				case "privacy-modal":
					ga('send', 'event', 'interactions', 'privacy', "");

					break;
				case "contact-modal":
					ga('send', 'event', 'interactions', 'contact', "");
					break;
			}

			if (modalId === 'terms-modal' && typeof triggerTag.attr('data-scroll-toggle') === 'string') {
				$("div[data-remodal-id=" + modalId + "]").addClass("remodal-medium").addClass("remodal-midheight").addClass("remodal-with-footer");
				$("div[data-remodal-id=" + modalId + "] .modal-footer").removeClass("remodal-hidden");
			} else if (modalId === 'terms-modal' && typeof triggerTag.attr('data-scroll-toggle') !== 'string') {
				$("div[data-remodal-id=" + modalId + "] .modal-footer").addClass("remodal-hidden");
				$("div[data-remodal-id=" + modalId + "]").removeClass("remodal-medium").removeClass("remodal-midheight").removeClass("remodal-with-footer");
			}

		};

		//attach click handlers for modal close links
		jTag.bind("click", loader);

	};

	// init orderSummary
	if (dataLineitem.length) {
		// example of how to update an item
		// orderSum.set('discount',-7.40).render();
		$.orderSum = new DomFields();
	}

	// attach data-panel config if data-panel-group entries present
	if(dataPanelGroup.length){
		tabPanels();
	}

	$('[data-modal-close]').on('click', function(){
		var tag = $(this);
		var modalId = tag.attr('data-modal-close');
		var inst = $("[data-remodal-id=" + modalId + "]").remodal();
		inst.close();
	});

	// display none check
	$('.remodal[style*="display: none"]').each(function(a,b){
		sashDebugList.warnings.push('need to remove display:none from style. sash handles this for you...');
		sashDebugList.warnings.push(b);
	});

	//attach handlers for general popup modals
	$('[data-remodal-target]:not(body)').each(function (a, tag) {
		//$('#' + tag.getAttribute('data-modal')).hide();
		popup(tag);
	});

	//attach handlers for exit pop modal
	$('body[data-modal]').each(function (a, tag) {
		exitpopup(tag);
	});

	// Hide modal on order submit

	// If we are on the checkout page, run checkout specific functions
	if ($("#billingForm").show) {

		// Attach stripe validation to credit card fields
		var CreditCardNumber = $('#creditCardNumber');
		var cvv = $('#CVV');
		var expiry = $('#exp');

		// Only bind these fields if we are on the billing page
		if (CreditCardNumber.length) {
			CreditCardNumber.payment('formatCardNumber');
			expiry.payment('formatCardExpiry');
			cvv.payment('formatCardCVC');
		}
		// Check if all credit card fields are valid when CVV is entered
		cvv.on('keyup', function() {
			checkLinkStatus();
		});

		// Check if all credit card fields are valid when expiration date is entered, if so enables checkout button
		expiry.on('change', function() {
			checkLinkStatus();
		});

		CreditCardNumber.on('keyup', function() {
			var ccNum = $(this).val();
			var creditCardType = $.payment.cardType(ccNum);

			// Detects the type of CC that the user entered. Alerts the user if the card type is accepted (match)
			// and displays an error if an unaccepted card type, like Diners Club, has been entered.
			switch (creditCardType) {
				case 'visa':
					$('#visa').addClass("active"); // highlights visa card
					$('#mastercard, #discover').removeClass("active"); // dims mc, discover
					$('#creditCardType').val("visa"); // sets hidden form field to visa
					break;
				case 'mastercard':
					$('#mastercard').addClass("active");
					$('#visa, #discover').removeClass("active");
					$('#creditCardType').val("master");
					break;
				case 'discover':
					$('#discover').toggleClass("active");
					$('#visa, #mastercard').removeClass("active");
					$('#creditCardType').val("discover");
					break;
				default: // Unknown credit card
					$('#visa, #mastercard, #discover').removeClass("active");
					$('#creditCardType').val("");
					break;
			}

			checkLinkStatus(); // checks if all credit card values are valid, if so enables checkout button

		});

		// Run as the credit card is entered
		CreditCardNumber.on('keyup', function() {
			var ccNum = $(this).val();


			if (!checkCreditCard(ccNum)) {
				// Restricted to 19 characters (16 + extra spaces added by masking), so we don't throw errors at the
				// customer as they enter in a number
				var ccType = "";

				if (! $.payment.cardType(ccNum)) {
					ccType = $.payment.cardType(ccNum);
				}
				if (ccNum.length === 19) {
					/* GA EVENT */
					ga('send', 'event', 'order-validation', 'ccNum-fail', ccType);
					displayCreditCardError(ccNum);
				}

			}

			checkLinkStatus();

		});

		CreditCardNumber.on('blur', function() {
			var ccNum = $(this).val();

			if (!checkCreditCard(ccNum)) {
				displayCreditCardError(ccNum);
			}

			checkLinkStatus();

		});


		var CVVStatus = $('#CVVStatus');
		// Checks to make sure that the CCV/CCV2 number entered matches the proper format for the card type that the user is using.
		cvv.on('blur change keyup', function() {

			var cvvVal = $(this).val();
			var creditCardType = $.payment.cardType(CreditCardNumber);

			if ($.payment.validateCardCVC(cvvVal, creditCardType)) {
				$(this).addClass("parsley-success").removeClass("parsley-error");
				/* GA EVENT */
				ga('send', 'event', 'order-validation', 'ccCVV-pass', "");
				CVVStatus.hide();
			} else {
				if ((cvvVal.length > 2 && !cvv.parsley().isValid()) || !cvv.is(':focus')) {
					$(this).removeClass("parsley-success").addClass("parsley-error");
					/* GA EVENT */
					ga('send', 'event', 'order-validation', 'ccCVV-fail', "");
					CVVStatus.html('<span class="error">Enter valid security code</span>').show();
				}
			}
			checkLinkStatus();
		});
		expiry.on('blur keyup', function() {
			var keyCheck = 0; /* 0 = do not check, 1 = check, 2 = remove all classes */
			var expVal = $(this).val();
			var expMonth = expVal.substring(0, 2);
			var expYear = expVal.substring(9, 4);
			var expStatus = $('#expStatus');

			// Each length check is stepped up by one to allow for masking
			if (expYear.length === 3 || expYear.length === 5 || !expiry.is(':focus')) {
				keyCheck = 1;
			} else if (expYear.length === 4) {
				keyCheck = 3;
			}

			if (keyCheck === 1) {
				if ($.payment.validateCardExpiry(expMonth, expYear)) {
					$(this).addClass("parsley-success").removeClass("parsley-error");
					/* GA EVENT */
					ga('send', 'event', 'order-validation', 'ccExpiry-pass', "");
					expStatus.hide();
				} else {
					$(this).removeClass("parsley-success").addClass("parsley-error");
					/* GA EVENT */
					ga('send', 'event', 'order-validation', 'ccExpiry-fail', "");
					expStatus.html('<span class="error">Enter valid expiration date</span>');
				}
			} else if (keyCheck === 3) {
				$(this).removeClass("parsley-success").removeClass("parsley-error");
			}

			checkLinkStatus();
		});

		$("#chkTermsConfirm").change(function() {
			checkLinkStatus();
		});

		// Check for address updates when the continue buttons are clicked
		$('button').click(function() {
			getAddresses();
		});

	}

	var CreditCardNumberStatus = $('#creditCardNumberStatus');

	// Determine whether to display a credit card error or whether to override for a test card
	function displayCreditCardError(ccNum) {
		var testCards = [
			"4444 3333 2222 1118",
			"4444 3333 2222 1119",
			"4444 3333 2222 1120"
		];

		if (testCards.indexOf(ccNum) !== -1) {
			CreditCardNumber.addClass("parsley-success").removeClass("parsley-error"); // Show success
			CreditCardNumberStatus.hide(); // Hide error container
		} else {
			CreditCardNumber.removeClass("parsley-success").addClass("parsley-error"); // Display parlsey errors to give user consistent experience
			CreditCardNumberStatus.html('<span class="error">Enter valid credit card</span>').show(); // Manually display error
		}

	}

	// Popupate billing and shipping address display fields
	function getAddresses() {

		var addressError = 0;
		var address = "";
		var shippingAddress = "";
		var billingAddress = "";
		var initialLoad = 1;

		for (var i = 0; i <= 1; i++) {

			// Reset address each time we loop through
			address = "";

			var addressType = "";

			if (i === 0) {
				addressType = "shipping";
			} else {
				addressType = "billing";
			}

			var tmpFirstName = getValue("#shippingFirstName");
			var tmpLastName = getValue("#shippingLastName");
			var tmpAddress1 = getValue("#" + addressType + "Address1");
			var tmpAddress2 = getValue("#" + addressType + "Address2");
			var tmpCity = getValue("#" + addressType + "City");
			var tmpState = getValue("#" + addressType + "State");
			var tmpZip = getValue("#" + addressType + "Zip");
			var tmpPhone = getValue("#" + addressType + "Phone");
			var tmpEmail = getValue("#" + addressType + "Email");

			// If we have all required fields, build out address
			if (!hasValue(tmpAddress1) && !hasValue(tmpCity) && !hasValue(tmpState) && !hasValue(tmpZip) && !hasValue(tmpPhone)) {
				addressError = 1;
			}

			if (addressType === "shipping" && (!hasValue(tmpFirstName) || !hasValue(tmpLastName) || !hasValue(tmpEmail))) {
				addressError = 1;
			}

			if (addressError === 0) {

				address += tmpFirstName + " " + tmpLastName + "<br />";
				address += tmpAddress1 + "<br />";

				if (hasValue(tmpAddress2)) {
					address += tmpAddress2 + "<br />";
				}

				address += tmpCity + ", " + tmpState + " " + tmpZip + "<br />";
				address += tmpPhone + "<br />";
				address += tmpEmail;
			} else { // Otherwise return error
				address = "Address incomplete.";
			}

			if (i === 0) {
				shippingAddress = address;
			} else {
				billingAddress = address;
			}

		}

		// Output addresses
		$("#shippingAddressPreview span").html(shippingAddress);

		if ($("#billingSameAsShipping").is(':checked')) {
			$("#billingAddressPreview span").html("Same as shipping.");
		} else {
			$("#billingAddressPreview span").html(billingAddress);
		}

	}

	function getValue(field) {
		var retrievedValue = $.trim($(field).val());
		var returnString = "";
		if (retrievedValue !== "undefined") {
			returnString = retrievedValue;
		} else {
			returnString = "";
		}
		return returnString;
	}

	function hasValue(field) {

		if (typeof field !== 'undefined') {
			if (field.length > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	// Checks to see if the CC number that the user entered is an accepted format.
	// It reports a match if true and sets the span to green, otherwise it remains transparent.
	function checkCreditCard(ccNum) {

		if ($.payment.validateCardNumber(ccNum) === true) {
			$("#creditCardNumber").addClass("parsley-success").removeClass("parsley-error");
			CreditCardNumberStatus.hide();
			return true;

		} else {
			return false;

		}

	}

	// Function to check if all credit card values are valid before enabling checkout button
	function checkLinkStatus() {
		var rushMyOrder = $('#submit-order');

		if (expiry.hasClass('parsley-success') && cvv.hasClass('parsley-success') && $('#creditCardNumber').hasClass('parsley-success')) {
			rushMyOrder.removeClass('linkDisabled');
		} else {
			rushMyOrder.addClass('linkDisabled');
		}

	}

};

debug = function(){

	var domDebugList = {
		warnings: [],
		errors: []
	};

	// unique DOM ID check
	var ids = {};
	$('[id]').map(function(){
		var key = $(this).attr('id');
		if(ids[key]){
			domDebugList.errors.push('duplicate DOM id ' + key);
			return false;
		}
		ids[key]=true;
	});

	// unknown DOM ID in data-remodal-target value
	$('[data-remodal-target]').map(function(){
		var value = $(this).attr('data-remodal-target');
		if(!$('#' + value).length){
			domDebugList.warnings.push('unknown DOM id ' + value + ' in data-remodal-target');
		}
	});

	// modal panel has no elements to fire it i.e. orphaned modal
	$('.remodal').map(function(){
		var key = $(this).attr('id');
		if(!$('[data-remodal-target=' + key + ']').length){
			domDebugList.warnings.push('modal panel ' + key + ' is not wired to an element to pop it');
		}
	});

	console.log('debugging on');
	console.group('sash');
	for(var i = 0; i<sashDebugList.warnings.length; i++ ){
		console.warn(sashDebugList.warnings[i]);
	}
	for(var i = 0; i<sashDebugList.errors.length; i++ ){
		console.error(sashDebugList.errors[i]);
	}
	console.groupEnd();

	console.group('DOM');
	for(var i = 0; i<domDebugList.warnings.length; i++ ){
		console.warn(domDebugList.warnings[i]);
	}
	for(var i = 0; i<domDebugList.errors.length; i++ ){
		console.error(domDebugList.errors[i]);
	}
	console.groupEnd();
};

$(function(){
	new Sash();
	// spit out console log stack generated by Sash init
	if($('body[data-debug]').length){
		debug();
	}

});

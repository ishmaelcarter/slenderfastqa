<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta content="nofollow, noindex" name="robots">
    <title>Slimera Garcinia Cambogia</title>

    <!-- Assets -->
    @include(theme_template("partials/assets"))
</head>
<body data-modal="exit-pop">
	<div id="process" class="hidden"><span>Processing<strong>Please Wait</strong></span></div>
    <header class="container">
        <div class="row benefit">
            <div class="col-xs-8 col-sm-8 col-xs-offset-4 col-sm-offset-4">
                <ul class="benefit-list icon-list checkmark">
                    <li><em>Inhibits <span>fat production</span></em></li>
                    <li><em>Suppresses <span>your appetite</span></em></li>
                    <li><em>Increases <span>serotonin levels for&nbsp;emotional&nbsp;eaters</span></em></li>
                </ul>
            </div>
        </div>

        <img id="h-body" width="200" src="{{ theme_asset("images/body.svg") }}" alt="" />
        <img id="h-hero" class="hidden-xs" width="260" src="{{ theme_asset("images/hero-31861058.png") }}" alt="" />
        <img id="h-product" width="180" src="{{ theme_asset("images/slimera-bottle-with-shadow.png") }}" alt="" />
        <img id="h-hca" width="70" src="{{ theme_asset("images/hca-badge.svg") }}" alt="" />
        <img id="h-natural" width="100" src="{{ theme_asset("images/natural.png") }}" alt="" />
        <img id="h-fda" width="120" src="{{ theme_asset("images/fda-badge.png") }}" alt="" />

    </header>
    <section id="main" class="container">


        <div class="row"><!-- container for two desktop columns -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Thanks for your order, <span>don't forget...</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <img id="vita-ultra" src={{ theme_asset("images/vita-ultra.png") }} />
                    </div>
                    <div class="col-xs-8">
                        <ul class="benefit-list icon-list checkmark">
                            <li><span>100%</span> Natural</li>
                            <li><span>Purify</span> your system</li>
                            <li><span>Reduce</span> harmful waste</li>
                            <li><span>Reduce</span> gas and bloating</li>
                            <li><span>Improve</span> daily digestion</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="lead text-center"><a href="#" data-remodal-target="vita-ultra">Click Here to Learn More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Vita Ultra <small>Cleanse your body today!</small></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table id="order-summary" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th class="text-right">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="lineitem">
                                <td>
                                    <div class="media">
                                        <a class="pull-left"><img width="50" src={{ theme_asset("images/vita-ultra-sm.png") }} alt="" /></a>
                                        <div class="media-body"><h4 class="media-heading">Vita Ultra</h4><p>18-Day Trial</p></div>
                                    </div>
                                </td>
                                <td class="text-right" style="font-weight: bolder;" data-lineitem="product" data-lineitem-empty="FREE">{{ $cart->shipping() }}</td>
                            </tr>

                            <tr class="lineitem"><td>Shipping &amp; Handling</td>
                                <td class="text-right" data-lineitem="shiphand">{{ $cart->subtotal() }}</td>
                            </tr>
                            <tr class="lineitem discount"><td>Discount Activated!</td>
                                <td class="text-right" data-lineitem="discount">{{ $cart->discount() * (-1) }}</td>
                            </tr>
                            <tr class="total"><td>Total</td>
                                <td class="total text-right" data-lineitem-total>{{ $cart->total() }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    @if($insurance)
                                    <a href="#gs-modal" data-modal="gs-modal"><img src={{ theme_asset("images/gship.png") }} width="100" alt="GuaranteeShip" /></a><small class="fineprint"> (Decline &nbsp;<input id="declineguaranteeship" type="checkbox" value="true">) +<span id="guarenteeship">${{ $insurance->price() }}</span><br />&nbsp;<i>billed separately</i></small>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <form id="billingform" action="/order" method="post">
                            @if($insurance)
                            <input type="hidden" name="shippingInsurance" id="shippingInsurance" value="1">
                            @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="_redirect" value="/thank-you" />
                            <button type="submit" class="btn btn-primary btn-lg btn-block gradient" id="yes-send-my-bottle">Yes, Send My Bottle! <span class="glyphicon glyphicon-triangle-right"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include( theme_template("partials/footer"))

    @include( theme_template("partials/terms"))
    @include( theme_template("partials/privacy"))
    @include( theme_template("partials/contact"))
	@include( theme_template("partials/vita"))
    @include( theme_template("partials/gs"))



    {{-- */
    $incentive = new \Worldwide\Russell\Products\Incentives\AmountOff(2.48);
    /*
    --}}
    @if($cart->canApplyIncentive($incentive) && !$cart->hasAppliedIncentive($incentive))
	<div class="remodal exit-pop" data-remodal-id="exit-pop">
	<button data-remodal-action="close" class="remodal-close"></button>
		<div class="exit-pop-content">
		<h1>Wait</h1>
		<h3>Only $2.48</h3>
			<div id="coupon">
				<h1>$2.49 off</h1>
				<h3>Order now for only $2.48</h3>
				<h5>You are only 1 step away from the body of your dreams!</h5>
			</div>
			<button data-remodal-action="close" data-incentive="amount:1" class="btn btn-primary btn-lg btn-block gradient" id="apply-coupon">Apply Coupon</button>
        </div>
    </div>
    @endif

<script>
    $('#declineguaranteeship').change(function(){

        if(this.checked){
            $('#guarenteeship').text('$0.00');
            $('#shippingInsurance').val(0);
        } else {
            $('#guarenteeship').text('{{ $insurance->price() }}');
            $('#shippingInsurance').val(1);
        }

    });

    $('[data-incentive]').on('click',function(){
        var incentive = $(this).attr('data-incentive').split(":");

        $.post('/api/incentive',
                {
                    '_token': "{{ csrf_token() }}",
                    type: incentive[0],
                    amount: incentive[1]
                },
                'json')
                .done(function(data){
                    if(data.success){
                        $.orderSum.set('discount', (data.cart.discount * (-1))).render();
                    }
                });
    });
</script>
@while($visitorSession->hasPixelsToFire())
    {!! $visitorSession->nextPixelToFire() !!}
@endwhile

</body>
</html>